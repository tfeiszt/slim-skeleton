<?php
use DI\Container;
use Slim\Factory\AppFactory;
use Noodlehaus\Config;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;

require __DIR__ . '/../../vendor/autoload.php';

// Create container instance
$container = new Container();

// Add Config instance to the container
$configs = [
    __DIR__ . '/../config/app.yaml',
    __DIR__ . '/../config/db.yaml',
];
if (is_file(__DIR__ . '/../../env.ini')) {
    $configs[] = __DIR__ . '/../../env.ini';
}
$conf =  Config::load($configs);
$container->set('Config', $conf);

// Add logger instance to the container
$logger = new Logger('applicationLogger');
$logger->pushHandler(new RotatingFileHandler(__DIR__ . '/../../tmp/logs/app.log', 0, $container->get('Config')->get('logger.level')));
$container->set('Logger', $logger);

// Create a PDO connection
$pdo = new \PDO( "mysql:host={$conf->get('db.host')}; dbname={$conf->get('db.name')}",
    $conf->get('db.user'),
    $conf->get('db.pwd')
);
$container->set('PDO', $pdo);

// Set the container
AppFactory::setContainer($container);

// Create Application
$app = AppFactory::create();

// Add middlewares
require __DIR__ . '/../middleware/middleware.php';

// Register routes
require __DIR__ . '/../http/routes.php';

// Run app
$app->run();
