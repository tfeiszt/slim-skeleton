<?php
namespace App\shared;

use tfeiszt\SqlBuilder\SqlConditions;

/**
 * Interface FilterableInterface
 * @package App\shared
 */
interface FilterableInterface
{
    /**
     * @param array $arguments
     * @return SqlConditions
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getConditions($arguments = []) : SqlConditions;

    /**
     * @param SqlConditions $conditions
     * @return int
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function countAll(SqlConditions $conditions) : int;
}
