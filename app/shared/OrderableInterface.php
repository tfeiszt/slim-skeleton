<?php
namespace App\shared;

/**
 * Interface OrderableInterface
 * @package App\shared
 */
interface OrderableInterface
{
    /**
     * @param array $arguments
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getOrder($arguments = []) : array;
}
