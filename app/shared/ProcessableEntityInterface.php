<?php
namespace App\shared;

/**
 * Interface ProcessableEntityInterface
 * @package App\shared
 */
interface ProcessableEntityInterface
{
    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getEntityName() : string;
}
