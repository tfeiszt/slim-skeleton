<?php
namespace App\shared;

/**
 * Interface ValidationInterface
 * @package App\shared
 */
interface ValidationInterface
{
    /**
     * @return bool
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function validate() : bool;

    /**
     * @param array $messages
     * @return ValidationInterface
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setErrorMessages($messages = []) : ValidationInterface;

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getErrorMessages() : array;
}
