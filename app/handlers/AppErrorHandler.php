<?php
namespace App\handlers;

use Slim\Handlers\ErrorHandler;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Slim\Interfaces\CallableResolverInterface;
use Psr\Http\Message\ResponseFactoryInterface;

/**
 * Class AppErrorHandler
 * @package App\handlers
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class AppErrorHandler extends ErrorHandler
{
    protected $logger;

    /**
     * AppErrorHandler constructor.
     * @param CallableResolverInterface $callableResolver
     * @param ResponseFactoryInterface $responseFactory
     * @throws \Exception
     */
    public function __construct(CallableResolverInterface $callableResolver, ResponseFactoryInterface $responseFactory)
    {
        parent::__construct($callableResolver, $responseFactory);
        $this->logger = new Logger('applicationLogger');
        $this->logger->pushHandler(new RotatingFileHandler(__DIR__ . '/../../tmp/logs/error.log', 0, \Monolog\Logger::ERROR));
    }

    /**
     * @param string $error
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    protected function logError(string $error): void
    {
        $this->logger->error($error);
    }

    /**
     * @return callable
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    protected function determineRenderer(): callable
    {
        return $this->callableResolver->resolve(AppErrorRenderer::class);
    }
}
