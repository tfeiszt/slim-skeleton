<?php

namespace App\handlers;

use Slim\Interfaces\ErrorRendererInterface;
use Throwable;

/**
 * Class AppErrorRenderer
 * @package App\handlers
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class AppErrorRenderer implements ErrorRendererInterface
{
    /**
     * @param Throwable $exception
     * @param bool $displayErrorDetails
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function __invoke(Throwable $exception, bool $displayErrorDetails): string
    {

        $error = ['message' => $exception->getMessage()];

        if ($displayErrorDetails) {
            $error['exception'] = [];
            do {
                $error['exception'][] = $this->formatExceptionFragment($exception);
            } while ($exception = $exception->getPrevious());
        }

        return (string) json_encode([
            'Error' => $error
        ], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }

    /**
     * @param Throwable $exception
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    private function formatExceptionFragment(Throwable $exception): array
    {
        return [
            'type' => get_class($exception),
            'code' => $exception->getCode(),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
        ];
    }
}