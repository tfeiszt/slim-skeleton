<?php
namespace App\model;

use App\shared\ProcessableEntityInterface;
use tfeiszt\DbSchema\AbstractTableDefinition;
use tfeiszt\DbSchema\Enum\AbstractDataType;
use tfeiszt\DbSchema\Enum\AbstractSchemaType;

/**
 * Class AuthProductKey
 * @package App\model
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class AuthProductKey extends AbstractTableDefinition implements ProcessableEntityInterface
{
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';

    const STATUS_ACTIVE = 'active';
    const STATUS_SUSPENDED = 'suspended';
    const STATUS_DELETED = 'deleted';

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getMapping()
    {
        return [
            'id' => AbstractDataType::INT_PRIMARY_KEY,
            'productKey' => AbstractDataType::CHAR_50,
            'name' =>AbstractDataType::STRING,
            'textDescription'=> AbstractDataType::TEXT,
            'role'=> AbstractDataType::CHAR_ENUM,
            'status' => AbstractDataType::CHAR_ENUM,
            'created' => AbstractDataType::DATE_TIME,
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function hasMany()
    {
        return [
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
     public static function manyToMany()
     {
         return [];
     }

    /**
     * @return string[]
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getIndices()
    {
        return [
            'unique_idx_product_key' => 'productKey',
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getDefaults()
    {
        return [
            'role' => static::ROLE_USER,
            'created' => function() {
                return Date('Y-m-d H:i:s');
                },
            'status' => static::STATUS_ACTIVE,
        ];
    }

    /**
     * @return string[]
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getFormats()
    {
        return [
            'created' => 'd/m/Y'
        ];
    }

    /**
     * @return \string[][]
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getEnumValues()
    {
        return [
            'active' => [
                static::STATUS_ACTIVE => 'Active',
                static::STATUS_DELETED => 'Deleted',
                static::STATUS_SUSPENDED => 'Suspended',
            ],
            'role' => [
                static::ROLE_ADMIN => 'Administrator',
                static::ROLE_USER => 'User'
            ]
        ];
    }

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getTablePrefix()
    {
        return '';
    }

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getPk()
    {
        return 'id';
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getSchemaType()
    {
        return AbstractSchemaType::MYSQL;
    }

    /**
     * @return \string[][]
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getDefaultOrderSet()
    {
        return [
            ['id', 'ASC']
        ];
    }

    /**
     * Returns an entity name for result processor (ProcessableEntityInterface)
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getEntityName(): string
    {
        return 'ProductKey';
    }
}
