<?php
namespace App\model;

use App\shared\FilterableInterface;
use App\shared\OrderableInterface;
use App\shared\ProcessableEntityInterface;
use App\shared\ValidationInterface;
use Respect\Validation\Exceptions\NestedValidationException;
use tfeiszt\DbSchema\AbstractTableDefinition;
use tfeiszt\DbSchema\Enum\AbstractDataType;
use tfeiszt\DbSchema\Enum\AbstractSchemaType;
use Respect\Validation\Validator as v;
use tfeiszt\SqlBuilder\Enum\AbstractSqlLikeCriteria;
use tfeiszt\SqlBuilder\SqlConditions;

/**
 * Class Example
 * @package App\model
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class Example extends AbstractTableDefinition implements
    ProcessableEntityInterface,
    FilterableInterface,
    OrderableInterface,
    ValidationInterface
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DELETED = 'deleted';

    protected $errorMessages = [];

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getMapping()
    {
        return [
            'id' => AbstractDataType::INT_PRIMARY_KEY,
            'name' =>AbstractDataType::STRING,
            'textDescription'=> AbstractDataType::TEXT,
            'status' => AbstractDataType::CHAR_ENUM,
            'created' => AbstractDataType::DATE_TIME,
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function hasMany()
    {
        return [
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function manyToMany()
    {
        return [];
    }

    /**
     * @return string[]
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getIndices()
    {
        return [];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getDefaults()
    {
        return [
            'created' => function() {
                return Date('Y-m-d H:i:s');
            },
            'status' => static::STATUS_ACTIVE,
        ];
    }

    /**
     * @return string[]
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getFormats()
    {
        return [
            'created' => 'd/m/Y'
        ];
    }

    /**
     * @return \string[][]
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getEnumValues()
    {
        return [
            'active' => [
                static::STATUS_ACTIVE => 'Active',
                static::STATUS_DELETED => 'Deleted',
            ],
        ];
    }

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getTablePrefix()
    {
        return '';
    }

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getPk()
    {
        return 'id';
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getSchemaType()
    {
        return AbstractSchemaType::MYSQL;
    }

    /**
     * @return \string[][]
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getDefaultOrderSet()
    {
        return [
            ['id', 'ASC']
        ];
    }

    /**
     * @param array $messages
     * @return $this
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setErrorMessages($messages = []) : ValidationInterface
    {
        $this->errorMessages = $messages;
        return $this;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getErrorMessages() : array
    {
        return $this->errorMessages;
    }

    /**
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function validate() : bool
    {
        $validator = v::attribute('name', v::stringType()->length(3, 255))
            ->attribute('text_description', v::stringType()->length(null, 1000))
            ->attribute('status', v::stringType()->in(static::STATUS_ACTIVE, static::STATUS_DELETED));
        try {
            $validator->assert((object)$this->__toAssoc());
        } catch (NestedValidationException $e) {
            $this->setErrorMessages($e->getMessages());
            return false;
        }
        return true;
    }

    /**
     * Returns an entity name for result processor (ProcessableEntityInterface)
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getEntityName(): string
    {
        return 'Example';
    }

    /**
     * @param array $arguments
     * @return SqlConditions
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getConditions($arguments = []): SqlConditions
    {
        $query = (isset($arguments['query'])) ? $arguments['query'] : '';
        if (!empty($query) && strlen($query) >=3) {
            $conditions = (new SqlConditions())->like('name', $query, AbstractSqlLikeCriteria::CONTAINS);
        } else {
            $conditions = (new SqlConditions())->add('true', []);
        }
        return $conditions;
    }

    /**
     * @param SqlConditions $conditions
     * @return int
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function countAll(SqlConditions $conditions): int
    {
        $countQuery = $this->getSelect()
            ->setConditions($conditions);
        $query = $this->pdo->prepare('SELECT COUNT(*) AS total FROM (' . $countQuery->toSql() . ') AS cnt');
        $query->execute($countQuery->getArgs());
        if ($total = $query->fetch($this->pdo::FETCH_ASSOC)) {
            return $total['total'];
        }
        return 0;
    }

    /**
     * @param array $arguments
     * @return string[]
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getOrder($arguments = []): array
    {
        // It could depend on arguments
        return [
            [
                'name',
                'ASC'
            ]
        ];
    }
}
