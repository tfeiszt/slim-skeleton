<?php

namespace App\middleware;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LogLevel;
use Slim\Exception\HttpException;

/**
 * Class AbstractAuthentication
 * @package App\middleware
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
abstract class AbstractAuthentication extends AbstractMiddleware implements MiddlewareInterface
{
    /**
     * @var array
     */
    protected $options = [
        'secure'      => true,
        'relaxed'     => ['localhost', '127.0.0.1'],
        'payload'     => 'product_key',
        'attribute'   => 'product_key',
        'log'         => true,
    ];


    /**
     * @param $productKey
     * @return bool
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract public function validate($productKey): bool;

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws HttpException
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function __invoke(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $this->process($request, $handler);
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws HttpException
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // Check security
        if (!$this->isSecure($request)) {
            return $this->unauthenticated($request, 'SSL Certificate Error', 495);
        }

        // Fetch to product key from the request and store as a request attribute
        $productKey = $this->fetchProductKey($request);
        $request = $request->withAttribute($this->options['attribute'], $productKey);

        // Validate the $productKey
        if (!$productKey || $this->validate($productKey) !== true) {
            return $this->unauthenticated($request, 'Invalid product key');
        }

        return $this->authenticated($request, $handler);
    }

    /**
     * @param ServerRequestInterface $request
     * @param $message
     * @param int $code
     * @return ResponseInterface
     * @throws HttpException
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function unauthenticated(ServerRequestInterface $request, $message, $code = 400): ResponseInterface
    {
        throw new HttpException($request, $message , $code);
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function authenticated(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $this->log(LogLevel::DEBUG, 'Request authenticated', [
            'product_key' => $request->getAttribute($this->options['attribute'])
        ]);
        return $handler->handle($request);
    }

    /**
     * @param ServerRequestInterface $request
     * @return bool
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function isSecure(ServerRequestInterface $request): bool
    {
        // No need if not set to be secure
        if ($this->options['secure'] === false) {
            return true;
        }

        // If this is a relaxed host
        if (in_array($request->getUri()->getHost(), $this->options['relaxed'])) {
            return true;
        }

        return $request->getUri()->getScheme() === 'https';
    }

    /**
     * @param ServerRequestInterface $request
     * @return mixed|string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function fetchProductKey(ServerRequestInterface $request)
    {
        $productKey = '';
        $postParams = $request->getParsedBody();
        if (is_array($postParams)) {
            $productKey = $postParams[$this->options['payload']] ?? '';
        } elseif (is_object($postParams)) {
            $productKey = $postParams->{$this->options['payload']} ?? '';
        }
        if (!is_string($productKey)) {
            return '';
        }
        return filter_var($productKey, FILTER_SANITIZE_STRING);
    }
}
