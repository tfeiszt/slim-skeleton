<?php

namespace App\middleware;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class AbstractAuthentication
 * @package App\middleware
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
abstract class AbstractMiddleware implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected $options = [];

    /**
     * AbstractMiddleware constructor.
     * @param ContainerInterface $container
     * @param array $options
     */
    public function __construct(ContainerInterface $container, array $options)
    {
        $this->container = $container;
        $this->options = array_merge($this->options, $options);
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface;

    /**
     * @param string $opt
     * @param null $default
     * @return mixed|null
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getOption(string $opt, $default = null)
    {
        return $this->options[$opt] ?? $default;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param $level
     * @param $message
     * @param array $context
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    protected function log($level, $message, array $context = []): void
    {
        if ($this->container->has('Logger') && $this->getOption('log')) {
            $this->container->get('Logger')->log($level, $message, $context);
        }
    }
}
