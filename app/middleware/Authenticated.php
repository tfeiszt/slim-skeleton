<?php
namespace App\middleware;

use App\model\AuthProductKey;
use Psr\Log\LogLevel;

/**
 * Class Authenticated
 * @package App\middleware
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class Authenticated extends AbstractAuthentication
{
    /**
     * @param $productKey
     * @return bool
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function validate($productKey): bool
    {
        try {
            // Find product key in Database
            $model = new AuthProductKey($this->container->get('PDO'));
            if ($model = $model->findFirstByProductKey($productKey)) {
                if ($model->status === AuthProductKey::STATUS_ACTIVE) {
                    return true;
                }
            }
            throw new \Exception('Invalid product key', 400);
        } catch (\Exception $exception) {
            $this->log(LogLevel::WARNING, $exception->getMessage(), ['product_key' => $productKey]);
            return false;
        }
        return false;
    }
}
