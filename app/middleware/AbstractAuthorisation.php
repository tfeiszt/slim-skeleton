<?php

namespace App\middleware;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LogLevel;
use Slim\Exception\HttpUnauthorizedException;

/**
 * Class AbstractAuthorisation
 * @package App\middleware
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
abstract class AbstractAuthorisation extends AbstractMiddleware implements MiddlewareInterface
{
    /**
     * @var array
     */
    protected $options = [
        'secure'      => true,
        'relaxed'     => ['localhost', '127.0.0.1'],
        'environment' => ['HTTP_AUTHORIZATION', 'REDIRECT_HTTP_AUTHORIZATION'],
        'header'      => 'X-Auth',
        'regex'       => '/(.*)/',
        'index'       => 1,
        'cookie'      => 'X-Auth',
        'payload'     => null,
        'attribute'   => 'token',
        'log'         => true,
    ];

    /**
     * @param $token
     * @return bool
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract public function validate($token): bool;

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws HttpUnauthorizedException
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function __invoke(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $this->process($request, $handler);
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws HttpUnauthorizedException
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // Check security
        if (!$this->isSecure($request)) {
            return $this->unauthorised($request);
        }

        // Fetch to token from the request and store as a request attribute
        $token = $this->fetchToken($request);
        $request = $request->withAttribute($this->options['attribute'], $token);

        // Validate the token
        if (!$token || $this->validate($token) !== true) {
            return $this->unauthorised($request);
        }

        return $this->authorised($request, $handler);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws HttpUnauthorizedException
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function unauthorised(ServerRequestInterface $request): ResponseInterface
    {
        throw new HttpUnauthorizedException($request);
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function authorised(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $this->log(LogLevel::DEBUG, 'Request Authorised', [
            'token' => $request->getAttribute($this->options['attribute'])
        ]);
        return $handler->handle($request);
    }

    /**
     * @param ServerRequestInterface $request
     * @return bool
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function isSecure(ServerRequestInterface $request): bool
    {
        // No need if not set to be secure
        if ($this->options['secure'] === false) {
            return true;
        }
        // If this is a relaxed host
        if (in_array($request->getUri()->getHost(), $this->options['relaxed'])) {
            return true;
        }

        return $request->getUri()->getScheme() === 'https';
    }

    /**
     * @param ServerRequestInterface $request
     * @return mixed|string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function fetchToken(ServerRequestInterface $request)
    {
        $token = '';

        // If using PHP in CGI mode and non-standard environment
        foreach ((array) $this->options['environment'] as $environment) {
            if (($token = $request->getServerParams()[$environment] ?? '') !== '') {
                break;
            }
        }

        // Fall back on the header name from the options array
        if (empty($token) && !empty($this->options['header'])) {
            $headers = $request->getHeader($this->options['header']);
            $token   = $headers[0] ?? '';
        }

        // Fall back on the payload
        if (empty($token) && !empty($this->options['payload'])) {
            $postParams = $request->getParsedBody();
            if (is_array($postParams)) {
                $token = $postParams[$this->options['payload']] ?? '';
            } elseif (is_object($postParams)) {
                $token = $postParams->{$this->options['payload']} ?? '';
            }
        }

        // Finally fall back on cookie
        if (empty($token) && !empty($this->options['cookie'])) {
            $token = $request->getCookieParams()[$this->options['cookie']] ?? '';
        }

        // Return the token
        if (!empty($token) && preg_match($this->options['regex'], $token, $matches)) {
            return $matches[$this->options['index']];
        } else {
            return '';
        }
    }
}
