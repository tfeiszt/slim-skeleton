<?php
use App\handlers\AppErrorHandler;

// Add Routing Middleware
$app->addRoutingMiddleware();

$app->addBodyParsingMiddleware();

// Add default error middleware
$errorMiddleware = $app->addErrorMiddleware(true, true, true);
$errorMiddleware->setDefaultErrorHandler(
    new AppErrorHandler($app->getCallableResolver(), $app->getResponseFactory())
);

// Add custom middlewares below...
