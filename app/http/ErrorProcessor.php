<?php
namespace App\http;

/**
 * Class ErrorProcessor
 * @package App\http
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class ErrorProcessor extends AbstractContentProcessor
{
    /**
     * @return array|false|string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getContent()
    {
        $error = [
            'message' => $this->data
        ];

        return json_encode([
            'Error' => $error
        ]);
    }
}
