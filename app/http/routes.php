<?php
use Slim\Routing\RouteCollectorProxy;
use App\middleware\Authenticated;
use App\middleware\Authorised;
use App\http\controller\StatusController;
use App\http\controller\TokenController;
use App\http\controller\ExampleController;

// Define endpoint root
$endpoint = sprintf('/%s/%s',
    $app->getContainer()->get('Config')->get('root'),
    $app->getContainer()->get('Config')->get('version')
);

// Define app routes
$app->group($endpoint, function (RouteCollectorProxy $group) use ($endpoint, $app) {
    $group->get('/status', StatusController::class . ':index');
    $group->post('/token', TokenController::class . ':create')
    ->add(new Authenticated($app->getContainer(), [
        // Use env.ini to override ssl locally instead of changing app.yaml
        'secure' => $app->getContainer()->get('Config')->get('protocol') == 'https://',
    ]));
    $group->get('/example', ExampleController::class . ':list')
        ->add(new Authorised($app->getContainer(), [
            // Use env.ini to override ssl locally instead of changing app.yaml
            'secure' => $app->getContainer()->get('Config')->get('protocol') == 'https://',
            'secret' => $app->getContainer()->get('Config')->get('jwt.secret'),
        ]));
    $group->get('/example/{id}', ExampleController::class . ':retrieve')
        ->add(new Authorised($app->getContainer(), [
            // Use env.ini to override ssl locally instead of changing app.yaml
            'secure' => $app->getContainer()->get('Config')->get('protocol') == 'https://',
            'secret' => $app->getContainer()->get('Config')->get('jwt.secret'),
        ]));
    $group->post('/example', ExampleController::class . ':create')
        ->add(new Authorised($app->getContainer(), [
            // Use env.ini to override ssl locally instead of changing app.yaml
            'secure' => $app->getContainer()->get('Config')->get('protocol') == 'https://',
            'secret' => $app->getContainer()->get('Config')->get('jwt.secret'),
        ]));
    $group->put('/example/{id}', ExampleController::class . ':update')
        ->add(new Authorised($app->getContainer(), [
            // Use env.ini to override ssl locally instead of changing app.yaml
            'secure' => $app->getContainer()->get('Config')->get('protocol') == 'https://',
            'secret' => $app->getContainer()->get('Config')->get('jwt.secret'),
        ]));
    $group->delete('/example/{id}', ExampleController::class . ':delete')
        ->add(new Authorised($app->getContainer(), [
            // Use env.ini to override ssl locally instead of changing app.yaml
            'secure' => $app->getContainer()->get('Config')->get('protocol') == 'https://',
            'secret' => $app->getContainer()->get('Config')->get('jwt.secret'),
        ]));
});
