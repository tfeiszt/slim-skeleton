<?php
namespace App\http;

use App\model\Example;
use App\shared\FilterableInterface;
use App\shared\OrderableInterface;
use App\shared\ProcessableEntityInterface;
use App\shared\ValidationInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use tfeiszt\DbSchema\Model\AbstractModel;
use tfeiszt\SqlBuilder\SqlConditions;

/**
 * Class AbstractCrudController
 * @package App\http
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
abstract class AbstractCrudController extends AbstractController
{
    /**
     * @var int
     */
    protected $defaultPageSize = 10;

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract protected function getModelClass();

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract protected function getProcessorClass();

    /**
     * @param $modelClass
     * @return AbstractModel
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    protected function modelFactory($modelClass = '')
    {
        $className = $this->getModelClass();
        if (!empty($modelClass) && class_exists($modelClass)) {
            $className = $modelClass;
        }
        return new $className($this->container->get('PDO'));
    }

    /**
     * @param $data
     * @param string $processorClass
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    protected function processorFactory($data, $processorClass = '')
    {
        $className = $this->getProcessorClass();
        if (!empty($processorClass) && class_exists($processorClass)) {
            $className = $processorClass;
        }
        return new $className($data);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function retrieve(Request $request, Response $response, $args = [])
    {
        $id = (isset($args['id'])) ? $args['id'] : '';
        if (empty($id)) {
            $response->withHeader('Content-Type', 'application/json')
                ->getBody()->write((new ErrorProcessor("Missing or invalid entity identification"))->getContent());
            return $response->withStatus(400);
        }

        $model = $this->modelFactory();
        if (!$instance = $model->findFirstById($id)) {
            $response->withHeader('Content-Type', 'application/json')
                ->getBody()->write((new ErrorProcessor("Entity has not found"))->getContent());
            return $response->withStatus(404);
        }

        $processor = $this->processorFactory($instance->__toAssoc());
        if ($instance instanceof ProcessableEntityInterface) {
            $processor->setEntityName($instance->getEntityName());
        }
        $response->withHeader('Content-Type', 'application/json')
            ->getBody()->write($processor->getContent());
        return $response->withStatus(200);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function create(Request $request, Response $response)
    {
        $args = $this->getPostData($request);
        if (empty($args)) {
            $response->withHeader('Content-Type', 'application/json')
                ->getBody()->write((new ErrorProcessor("No data received"))->getContent());
            return $response->withStatus(400);
        }

        $model = $this->modelFactory();

        try {
            $model->setFormData($args);
            // Validation here
            if ($model instanceof ValidationInterface) {
                if (!$model->validate()) {
                    throw new \Exception(implode("|", $model->getErrorMessages()), 400);
                }
            }
            if (!$instance = $model->save()) {
                throw new \Exception('Saving error', 400);
            }
        } catch (\Exception $e) {
            $response->withHeader('Content-Type', 'application/json')
                ->getBody()->write((new ErrorProcessor($e->getMessage()))->getContent());
            return $response->withStatus(($e->getCode()) ?? 500);
        }

        $processor = $this->processorFactory($instance->__toAssoc());
        if ($instance instanceof ProcessableEntityInterface) {
            $processor->setEntityName($instance->getEntityName());
        }
        $response->withHeader('Content-Type', 'application/json')
            ->getBody()->write($processor->getContent());
        return $response->withStatus(201);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function update(Request $request, Response $response, $args = [])
    {
        $id = (isset($args['id'])) ? $args['id'] : '';
        if (empty($id)) {
            $response->withHeader('Content-Type', 'application/json')
                ->getBody()->write((new ErrorProcessor("Missing or invalid entity identification"))->getContent());
            return $response->withStatus(400);
        }

        $args = $this->getPostData($request);
        if (empty($args)) {
            $response->withHeader('Content-Type', 'application/json')
                ->getBody()->write((new ErrorProcessor("No data has received"))->getContent());
            return $response->withStatus(400);
        }

        $model = $this->modelFactory();

        if (!$instance = $model->findFirstById($id)) {
            $response->withHeader('Content-Type', 'application/json')
                ->getBody()->write((new ErrorProcessor("Entity has not found"))->getContent());
            return $response->withStatus(404);
        }

        try {
            $instance->setFormData($args);
            // Validation here
            if ($model instanceof ValidationInterface) {
                if (!$instance->validate()) {
                    throw new \Exception(implode("|", $instance->getErrorMessages()), 400);
                }
            }
            if (!$instance = $instance->save()) {
                throw new \Exception('Saving error', 400);
            }
        } catch (\Exception $e) {
            $response->withHeader('Content-Type', 'application/json')
                ->getBody()->write((new ErrorProcessor($e->getMessage()))->getContent());
            return $response->withStatus(($e->getCode()) ?? 500);
        }

        $processor = $this->processorFactory($instance->__toAssoc());
        if ($instance instanceof ProcessableEntityInterface) {
            $processor->setEntityName($instance->getEntityName());
        }
        $response->withHeader('Content-Type', 'application/json')
            ->getBody()->write($processor->getContent());
        return $response->withStatus(202);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function delete(Request $request, Response $response, $args = [])
    {
        $id = (isset($args['id'])) ? $args['id'] : '';
        if (empty($id)) {
            $response->withHeader('Content-Type', 'application/json')
                ->getBody()->write((new ErrorProcessor("Missing or invalid entity identification"))->getContent());
            return $response->withStatus(400);
        }

        $model = $this->modelFactory();
        if (!$instance = $model->findFirstById($id)) {
            $response->withHeader('Content-Type', 'application/json')
                ->getBody()->write((new ErrorProcessor("Entity has not found"))->getContent());
            return $response->withStatus(404);
        }

        try {
            if (!$instance->delete()) {
                throw new \Exception('Delete has not been successful', 400);
            }
        } catch (\Exception $e) {
            $response->withHeader('Content-Type', 'application/json')
                ->getBody()->write((new ErrorProcessor($e->getMessage()))->getContent());
            return $response->withStatus(($e->getCode()) ?? 500);
        }

        $response->withHeader('Content-Type', 'application/json')
            ->getBody()->write(json_encode([]));
        return $response->withStatus(204);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function list(Request $request, Response $response)
    {
        /** @var Example $model */
        $model = $this->modelFactory();
        $argv = $this->sanitize($request->getQueryParams());
        $page = (isset($argv['page'])) ? (int) $argv['page'] : 1;
        $size = (isset($argv['size'])) ? (int) $argv['size'] : $this->defaultPageSize;
        $orders = [
            [
                $model->getPk() ,
                'ASC',
            ]
        ]; // Order by ID by default
        if ($model instanceof OrderableInterface) {
            $orders = $model->getOrder($argv);
        }
        if ($model instanceof FilterableInterface) {
            $conditions = $model->getConditions($argv);
        } else {
            $conditions = (new SqlConditions())->add('true', []); // an empty sql condition
        }

        $collection = false;
        if ($total = $model->countAll($conditions)) {
            // Ordered, limited select
            $collection = $model->findAll(
                $conditions,
                $page,
                $size,
                $orders
            );
        }

        if ($collection == false) {
            $collection = [];
        }
        $processor = new CollectionProcessor($collection);
        if ($model instanceof ProcessableEntityInterface) {
            $processor->setEntityName($model->getEntityName());
        }
        $processor->setPagination((int) $page, (int) $size, (int) $total);

        $response->withHeader('Content-Type', 'application/json')
            ->getBody()->write($processor->getContent());
        return $response->withStatus(200);
    }
}
