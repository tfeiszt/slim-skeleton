<?php
namespace App\http;

/**
 * Class JsonProcessor
 * @package App\http
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class JsonProcessor extends AbstractContentProcessor
{
    /**
     * @var string
     */
    protected $entityName = 'Entity';

    /**
     * JsonProcessor constructor.
     * @param $data
     */
    public function __construct($data)
    {
        if (!is_array($data)) {
            $data = [$data];
        }
        parent::__construct($data);
    }

    /**
     * @return string|false
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getContent()
    {
        return json_encode([
            $this->entityName => $this->data
        ]);
    }

    /**
     * @param string $entityName
     * @return $this
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setEntityName(string $entityName)
    {
        $this->entityName = $entityName;
        return $this;
    }
}
