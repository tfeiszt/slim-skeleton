<?php
namespace App\http;

/**
 * Class JsonProcessor
 * @package App\http
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
abstract class AbstractContentProcessor
{
    /**
     * @var array
     */
    protected $data;

    /**
     * AbstractContentProcessor constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return array|string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract public function getContent();
}
