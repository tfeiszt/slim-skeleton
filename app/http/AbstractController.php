<?php
namespace App\http;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class AbstractController
 * @package App\http
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
abstract class AbstractController
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * constructor receives container instance
     * AbstractController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @param bool $sanitize
     * @return array|mixed|object|null
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    protected function getPostData(Request $request, $sanitize = true)
    {
        $postParams = $request->getParsedBody();
        if (is_array($postParams)) {
            if ($sanitize === true) {
                return $this->sanitize($postParams);
            }
        } elseif (is_object($postParams)) {
            $postParams = (array) $postParams;
        }
        if ($sanitize === true) {
            return $this->sanitize($postParams);
        }
        return $postParams;
    }

    /**
     * @param array $data
     * @return array|mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    protected function sanitize($data)
    {
        if (is_array($data)) {
            foreach ($data as $k => $val) {
                $data[$k] = filter_var($val, FILTER_SANITIZE_STRING);
            }
        } else {
            $data = filter_var($data, FILTER_SANITIZE_STRING);
        }
        return $data;
    }
}
