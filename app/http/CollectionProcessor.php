<?php
namespace App\http;

use tfeiszt\DbSchema\Model\AbstractModel;

/**
 * Class CollectionProcessor
 * @package App\http
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class CollectionProcessor extends JsonProcessor
{
    /**
     * @var array
     */
    protected $pagination = [];

    /**
     * @return array|false|string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getContent()
    {
        $result = [
            $this->entityName . 'Collection' => [
                'Collection' => []
            ]
        ];
        /** @var AbstractModel $entity */
        foreach ($this->data as $entity) {
            $result[$this->entityName . 'Collection']['Collection'][] = $entity->__toAssoc();
        }
        if (count($this->pagination)) {
            $result[$this->entityName . 'Collection']['Pagination'] = $this->pagination;
        }
        return json_encode($result);
    }

    /**
     * @param int $page
     * @param int $size
     * @param int $total
     * @return $this
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setPagination(int $page, int $size, int $total)
    {
        $this->pagination = [
            'page' => $page,
            'size' => $size,
            'total' => $total
        ];
        return $this;
    }
}
