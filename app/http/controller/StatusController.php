<?php
namespace App\http\controller;

use App\http\AbstractController;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class StatusController
 * @package App\http\controller
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class StatusController extends AbstractController
{
    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function index(Request $request, Response $response)
    {
        $response->withHeader('Content-Type', 'application/json')
            ->getBody()->write(json_encode(["Status" => "OK"]));
        return $response->withStatus(200);
    }
}
