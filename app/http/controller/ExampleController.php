<?php
namespace App\http\controller;

use App\http\AbstractCrudController;
use App\model\Example;
use App\http\JsonProcessor;

/**
 * Class ExampleController
 * @package App\http\controller
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class ExampleController extends AbstractCrudController
{
    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    protected function getModelClass()
    {
        return Example::class;
    }

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    protected function getProcessorClass()
    {
        return JsonProcessor::class;
    }
}
