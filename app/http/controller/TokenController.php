<?php
namespace App\http\controller;

use App\http\AbstractController;
use App\http\ErrorProcessor;
use Firebase\JWT\JWT;
use App\http\JsonProcessor;
use App\model\AuthProductKey;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class TokenController
 * @package App\http\controller
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class TokenController extends AbstractController
{
    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function create(Request $request, Response $response)
    {
        $productKey = $request->getAttribute('product_key'); // Sanitized. Coming from authentication middleware.
        $model = new AuthProductKey($this->container->get('PDO'));
        if (!$instance = $model->findFirstByProductKey($productKey)) {
            $response->withHeader('Content-Type', 'application/json')
                ->getBody()->write((new ErrorProcessor('Invalid product key'))->getContent());
            return $response->withStatus(404);
        }

        $token = $this->createTokenByKey($instance);

        $processor = (new JsonProcessor($token))->setEntityName('Token');

        $response->withHeader('Content-Type', 'application/json')
            ->withStatus(201)
            ->getBody()->write($processor->getContent());
        return $response;
    }

    /**
     * @param AuthProductKey $key
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    protected function createTokenByKey(AuthProductKey $key)
    {
        $dt = new \DateTime();
        $expiry = (int) $this->container->get('Config')->get('jwt.expiry');
        if ($expiry < 60) {
            $expiry = 60; // 60 sec minimum
        }
        $payload = [
            'sub' => $key->id,
            'name' => $key->name,
            'role' => $key->role,
            'jti' => $key->productKey,
            'iat' => $dt->getTimestamp(),
            'exp' => $dt->getTimestamp() + $expiry,
        ];

        return [
            'access_token' => JWT::encode($payload, $this->container->get('Config')->get('jwt.secret')),
            'token_type' => 'Bearer',
            'expires_in' => $expiry,
        ];
    }
}
