<?php
require_once __DIR__. '/../../vendor/autoload.php';
// Read db credentials config
$configs = [
    __DIR__ . '/../config/db.yaml',
];
// Read env overrides
if (is_file(__DIR__ . '/../../env.ini')) {
    $configs[] = __DIR__ . '/../../env.ini';
}
$conf = \Noodlehaus\Config::load($configs);

$pos = array_search('--key', $argv);
// Check mandatory options
if ($pos === false || !isset($argv[$pos+1])) {
    echo "Parameter --key is required. \n";
    echo "Usage: php create_auth_key.php --key XXXX-XXXX-XXXX-XXXX \n";
    exit;
}
$key = $argv[$pos+1];

// Create a PDO connection
$pdo = new \PDO( "mysql:host={$conf->get('db.host')}; dbname={$conf->get('db.name')}",
    $conf->get('db.user'),
    $conf->get('db.pwd')
);

// Create an AuthProductKey instance
$model = new \App\model\AuthProductKey($pdo);

// Check is given key unique
if ($existing = $model->findFirstByProductKey($key)) {
    echo "Error. Given key is already exists.";
    exit;
}
// Save the given key
$model->setFormData([
    'product_key' => $key,
    'name' => 'Example key',
    'role' => \App\model\AuthProductKey::ROLE_USER,
    'status' => \App\model\AuthProductKey::STATUS_ACTIVE,
]);
if (!$model->save()) {
    echo "Error. Saving the auth key was not successful.";
}
echo "Auth key created:\n";
echo json_encode($model->__toAssoc());
echo "\nDone\n";
