<?php
require_once __DIR__. '/../../vendor/autoload.php';
// Read db credentials config
$configs = [
    __DIR__ . '/../config/db.yaml',
];
// Read env overrides
if (is_file(__DIR__ . '/../../env.ini')) {
    $configs[] = __DIR__ . '/../../env.ini';
}
$conf = \Noodlehaus\Config::load($configs);

// Verbose
$show = in_array('--show-script', $argv);

// No update option
$scriptOnly = in_array('--generate-script-only', $argv);

// Create a PDO connection
$pdo = new \PDO( "mysql:host={$conf->get('db.host')}; dbname={$conf->get('db.name')}",
    $conf->get('db.user'),
    $conf->get('db.pwd')
);

// Create a new DB schema instance
$schema = new \App\model\Schema($pdo);

echo "Updating schema\n";
// Updating the database. It creates new tables, fields, indices, drops old indices. It doesn't drop any unregistered tables or removed fields.
$script = $schema->update($scriptOnly);
if ($show || $scriptOnly) {
    echo "\n";
    echo "Script:\n";
    echo "\n";
    echo $script->toSql();
    echo "\n";
}
echo "Done\n";
