# Slim 4.x based microservice skeleton

### Description
This is a skeleton framework based on SLIM 4.x for building CRUD microservices.

### Installation

1. Via git
    ````
    git clone git@bitbucket.org:tfeiszt/slim-skeleton.git
    ````
2. Install packages:
    ````
    composer install
    ````

3. Create a database, and modify application host, db name and credentials in following files:
    - app/config/app.yaml
    - app/config/db.yaml
    
    OR
    
    - Create an env.ini file into the project root instead of editing the yaml files.
    
    For example:
   
    protocol=http://

    site=<yourhost.local>
   
    db.host=<your host>
   
    db.name=<your db name>
   
    db.user=<db user>
   
    db.pwd=<db password>


4. Migration:
    ````
    php ./app/command/update_schema.php
    ````
5. Create a product key:
   ````
    php ./app/command/create_auth_key.php --key <YOUR_PRODUCT_KEY_HERE>
   ````
   
### Endpoints
Requesting access token: 
````
curl -H 'Accept: application/json' -H 'Content-Type: application/json' -d '{"product_key":"<YOUR_PRODUCT_KEY_HERE>"'} http://slim.local/rest/v1/token
````
Retrieve an "example" entity
```
curl -H 'Accept: application/json' -H "Authorization: Bearer <YOUR_TOKEN_HERE>" http://slim.local/rest/v1/example/<ID>
```
Listing an "example" collection (querystring parameters are optional)
```
curl -H 'Accept: application/json' -H "Authorization: Bearer <YOUR_TOKEN_HERE>" http://slim.local/rest/v1/example?page=1&size=10
```
Create an "example" entity
````
curl -H 'Accept: application/json' -H 'Content-Type: application/json' -H "Authorization: Bearer <YOUR_TOKEN_HERE>" http://slim.local/rest/v1/example -d "{\"name\": \"some name\",\"text_description\": \"some description\", \"status\": \"active\"}"
````
Modify "example" entity
````
curl -X PUT -H 'Accept: application/json' -H 'Content-Type: application/json' -H "Authorization: Bearer <YOUR_TOKEN_HERE>" http://slim.local/rest/v1/example/<ID> -d "{\"name\": \"modified name\",\"text_description\": \"some modified description\", \"status\": \"active\"}"
````
Remove an "example" entity
````
curl -X DELETE -H 'Accept: application/json' -H "Authorization: Bearer <YOUR_TOKEN_HERE>" http://slim.local/rest/v1/example/<ID>
````

### What's inside?

* - Config
* - Routing examples
* - Authentication middleware
* - Authorisation middleware using JWT tokens
* - Logging
* - Abstract controller for basic CRUD endpoints
* - ORM based data provider with database migration (See [tfeiszt/dbschema](https://bitbucket.org/tfeiszt/dbschema/src/master/))
* - Basic models

See [Slim 4 documentation](https://www.slimframework.com/docs/v4/)

### "Make your own kind of music"
1. Fork the master branch of this repository
2. Develop your custom features
3. Enjoy

### License

MIT

